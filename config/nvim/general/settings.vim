set number relativenumber
set laststatus=0
set smartindent
set autoindent
set expandtab
set smarttab
set tabstop=4
set shiftwidth=4
set showtabline=4
set formatoptions-=cro
set hidden
set encoding=utf-8
set fileencoding=utf-8
set ruler
set mouse=a
set cursorline
set splitbelow
set splitright
set clipboard=unnamedplus
set background=dark
set autochdir
set nowrap
syntax enable
filetype on
filetype indent on
filetype plugin on

let g:head = [
            \
            \ ' ██╗   ██╗██╗███╗   ███╗',
            \ ' ██║   ██║██║████╗ ████║',
            \ ' ██║   ██║██║██╔████╔██║',
            \ ' ╚██╗ ██╔╝██║██║╚██╔╝██║',
            \ '  ╚████╔╝ ██║██║ ╚═╝ ██║',
            \ '   ╚═══╝  ╚═╝╚═╝     ╚═╝',
          \]
let g:startify_custom_header = g:head

let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   file']            },
          \ { 'type': 'dir',       'header': ['   dirs']            },
          \ { 'type': 'commands',  'header': ['   cmds']            },
          \ ]
let g:startify_files_number = 5
let g:startify_padding_left = 3
let g:startify_custom_indices = []
let g:startify_custom_indices = ['', '', '', '', '', '', '', '', '', '']
